class ApplicationController < ActionController::API
	#before_action :authenticate_request
	attr_reader :current_user
	attr_reader :current_gm
	
	private

	def authenticate_request
		puts request.headers
		@current_user = AuthorizeApiRequest.call(request.headers).result
		@current_gm = AuthorizeGmApiRequest.call(request.headers).result
		
		#if either @current_user or @current_gm is valid
		render json: { error: 'Not Authorized' }, status: 401 unless @current_user || @current_gm
	end
end
