class AuthenticateGm
	prepend SimpleCommand

	def initialize(email, password)
		@email = email
		@password = password
	end

	def call
		JsonWebToken.encode(gm_id: gm.id) if gm
	end

	private

	attr_accessor :email, :password

	def gm
		gm = Gm.find_by_email(email)
		return gm if gm && gm.authenticate(password)

		errors.add :gm_authentication, 'invalid credentials'
		nil
	end
end