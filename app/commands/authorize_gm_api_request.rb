class AuthorizeGmApiRequest
	prepend SimpleCommand

	def initialize(headers = {})
		@headers = headers
	end

	def call
		gm
	end

	private

	attr_reader :headers

	def gm
		if decoded_auth_token
			if decoded_auth_token[:gm_id] != nil
				@gm ||= Gm.find_by_id(decoded_auth_token[:gm_id]) if decoded_auth_token
				@gm || errors.add(:token, 'Invalid token') && nil
			else
				@gm ||= Gm.find_by_id(-1)
				@gm || errors.add(:token, 'Invalid token') && nil
			end
		end
	end

	def decoded_auth_token
		@decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
	end

	def http_auth_header
		if headers['authorization'].present?
			return headers['authorization'].split(' ').last
		else
			errors.add(:token, 'Missing token')
		end
		nil
	end
end
