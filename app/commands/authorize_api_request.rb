class AuthorizeApiRequest
	prepend SimpleCommand

	def initialize(headers = {})
		@headers = headers
	end

	def call
		user
	end

	private

	attr_reader :headers

	def user
		if decoded_auth_token
			if decoded_auth_token[:user_id] != nil
				@user ||= User.find_by_id(decoded_auth_token[:user_id]) if decoded_auth_token
				@user || errors.add(:token, 'Invalid token') && nil
			else
				@user ||= User.find_by_id(-1)
				@user || errors.add(:token, 'Invalid token') && nil
			end
		end
	end

	def decoded_auth_token
		@decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
	end

	def http_auth_header
		if headers['authorization'].present?
			return headers['authorization'].split(' ').last
		else
			errors.add(:token, 'Missing token')
		end
		nil
	end
end
