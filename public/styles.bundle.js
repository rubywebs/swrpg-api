webpackJsonp(["styles"],{

/***/ "../../../../../src/styles.css":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/styles.css");
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__("../../../../style-loader/addStyles.js")(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../node_modules/css-loader/index.js??ref--8-1!../node_modules/postcss-loader/index.js??postcss!./styles.css", function() {
			var newContent = require("!!../node_modules/css-loader/index.js??ref--8-1!../node_modules/postcss-loader/index.js??postcss!./styles.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/styles.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* You can add global styles to this file, and also import other style files */\nbody\n{\n\tbackground-color: rgba(234, 235, 236, 1);\n}\n\n.character-heading\n{\n    color: rgb(169, 69, 68);\n    font-family: 'Biryani', sans-serif;\n}\n\n.emp-color\n{\n    color: rgb(160, 60, 68);\n    font-weight: bold;\n}\n\n.character-section\n{\n    background-color: white;\n    border-bottom: 1px solid rgb(200, 200, 200);\n    border-right: 1px solid rgb(200, 200, 200);\n    width: 100%;\n    margin: 5px;\n}\n\n.character-overview-section\n{\n    width: 45%;\n    float: left;\n}\n\nimg.nav-header\n{\n\twidth: 100%;\n\tmargin-bottom: 10px;\n\tmargin-top: 10px;\n\tborder: solid 1px silver;\n\tborder-radius: 5px;\n}\n\nimg.nav-item\n{\n\twidth: 100%;\n\tmargin-bottom: 5px;\n\tborder: solid 1px silver;\n\tborder-radius: 10px;\n}\n\nimg.main-nav\n{\n  width: 100%;\n}\n\nimg.pc-nav\n{\n\twidth: 100%;\n}\n\nimg.gm-button\n{\n\twidth: 98%;\n\tmargin-bottom: 5px;\n\tborder-radius: 3px;\n\tmargin-left: 1%;\n\tmargin-right: 1%;\n}\n\n.add-button\n{\n\twidth: 100%;\n\theight: 32px;\n\tbackground-image: image_url('add_button_bg.png');\n\tbackground-size: 100%;\n\tbackground-repeat: no-repeat;\n\tcolor: black;\n\tfont-size: 20px;\n}\n.horizontal-line\n{\n\twidth: 100%\n}\n\n.circle-add, .circle-delete\n{\n\twidth: 32px;\n\theight: 32px;\n}\n\n.circle-add-sm, .circle-delete-sm\n{\n    width: 16px;\n    height: 16px;\n}\n\n.table-margin\n{\n\tmargin: 0px;\n}\n\ndiv.main-nav-small\n{\n  margin-top: 10px;\n}\n\ndiv#enter\n{\n\twidth: 70%;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\ttext-align: center;\n}\n\nsection#gm-content2\n{\n\tbackground-color: rgba(20,10,10,0.90);\n}\n\ndiv#player\n{\n\tmin-width: 320px;\n}\n\nform#pc-sign-in\n{\n\twidth: 40%;\n\tmin-width: 300px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n\nform#editplayer\n{\n\twidth: 40%;\n\tmin-width: 300px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n\ndiv#characterselected\n{\n\t//width: 80%;\n\t//margin-left: 10%;\n\t//margin-right: 10%;\n\twidth: 100%;\n    margin-top: 50px;\n}\n\ndiv#characterselect img\n{\n\twidth: 100%;\n}\n\nimg.character-avatar\n{\n\twidth: 10%;\n\tmin-width: 100px;\n\tfloat: left;\n\tmargin: 10px;\n}\n\nimg#avatar-preview\n{\n  width: 100%;\n  margin: 10px;\n}\n\ntable#character-stats-preview\n{\n\tmin-width: 320px;\n\tmax-width: 500px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\t\n\tfont-size: 10px;\n}\n\ntable#skills\n{\n\tmin-width: 320px;\n\tmax-width: 500px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\t\n\tfont-size: 11px;\n}\n\ntable#attributes\n{\n\tmin-width: 320px;\n\tmax-width: 500px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n\t\n\tfont-size: 11px;\n}\n\nth#career\n{\n\twidth: 10%;\n}\n\nth#skill\n{\n\twidth: 60%;\n}\n\nth#attribute\n{\n\twidth: 25%;\n}\n\nth#rank\n{\n\twidth: 5%;\n}\n\ntd#skill-rank\n{\n\twidth: 5%;\n}\n\ntd.table-rank\n{\n\twidth: 5%;\n\tborder-right: 1px solid black;\n}\n\n.btn-increase\n{\n\tcolor: green;\n\tborder: solid 1px green;\n\tborder-radius: 10px;\n\tbackground-color: greenyellow;\n\tfloat: right;\n}\n\n.btn-remove\n{\n\tborder: solid 1px red;\n\tborder-radius: 10px;\n\tbackground-color: red;\n\t//font-size: 10px;\n\tfont-weight: bold;\n}\n\n.select_character\n{\n\tborder: solid 1px rgb(179,3,3);\n}\n\np\n{\n\tfont-family: 'Source Sans Pro', sans-serif;\n}\n\nul#character-nav\n{\n  background-color: tan;\n  margin-left: auto;\n  margin-right: auto;\n  max-width: 800px;\n  min-width: 320px;\n  height: 50px;\n}\n\nul#character-nav li\n{\n\tfont-family: 'Kalam', cursive;\n\tfont-size: 35px;\n  list-style: none;\n  float: left;\n  width: 25%;\n  height: 20px;\n  padding: 0px;\n  margin: 0px;\n}\n\nul#character-nav li a\n{\n\ttext-decoration: none;\n\tcolor: black;\n}\n\nul#character-nav li a:hover\n{\n\ttext-decoration: none;\n\tcolor: blue;\n}\n\nimg.dice\n{\n\theight: 20px;\n}\n\n.float-left\n{\n\tfloat: left;\n}\n\n.float-right\n{\n\tfloat: right;\n}\n\ndiv#character_skills\n{\n\tmax-height: 600px;\n\toverflow-y: scroll;\n}\n\ndiv#character_overview p\n{\n\tfont-family: 'Rock Salt', cursive;\n\tfont-size: 12px;\n}\n\n.gm-all-pcs\n{\n\twidth: 18%;\n\tfloat: left;\n\tborder: 1px solid black;\n\tborder-radius: 5px;\n\tmargin: 5px;\n\tbackground-color: white;\n\tpadding: 5px;\n}\n\ntd.rank input.ng-dirty\n{\n\ttransition: 1s linear all;\n\tfont-weight: bold;\n\tbackground-color: rgb(54, 255, 48);\n}\n\ntable#pc_attributes\n{\n\tmargin-top: 20px;\n}\n\ntable#pc_attributes input.ng-dirty\n{\n\ttransition: 1s linear all;\n\tfont-weight: bold;\n\tbackground-color: rgb(54, 255, 48);\n}\n\nul#gm_pc_update\n{\n\twidth: 100%;\n\theight: 30px;\n\tlist-style: none;\n\t\n}\n\nul#gm_pc_update li\n{\n\tfloat: left;\n\tpadding: 10px;\n\ttext-align: center;\n}\n\n.margin-zero\n{\n  margin: 0px;\n}\n\n#game_session\n{\n\tcolor: gold;\n\ttransition:0.5s linear all;\n}\n\n.session_pcs\n{\n  border: 1px solid black;\n}\n\n.session_avatar\n{\n  width: 100%;\n}\n\n.manage_pcs\n{\n transition:0.5s linear all;\n opacity:1;\n\n}\n\n.manage_pcs.ng-hide\n{\n  transition:0.5s linear all;\n  opacity:0;\n}\n\ntr.all_pcs.ng-leave\n{\n\ttransition:0.25s linear all;\n\topacity:1;\n}\ntr.all_pcs.ng-leave.ng-leave-active\n{\n\topacity:0;\n}\n\n.border-bottom\n{\n\tborder-bottom: 1px solid black;\n}\n\n#admin_nav\n{\n\tlist-style: none;\n}", ""]);

// exports


/***/ }),

/***/ "../../../../css-loader/lib/css-base.js":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "../../../../style-loader/addStyles.js":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
var stylesInDom = {},
	memoize = function(fn) {
		var memo;
		return function () {
			if (typeof memo === "undefined") memo = fn.apply(this, arguments);
			return memo;
		};
	},
	isOldIE = memoize(function() {
		return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
	}),
	getHeadElement = memoize(function () {
		return document.head || document.getElementsByTagName("head")[0];
	}),
	singletonElement = null,
	singletonCounter = 0,
	styleElementsInsertedAtTop = [];

module.exports = function(list, options) {
	if(typeof DEBUG !== "undefined" && DEBUG) {
		if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};
	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (typeof options.singleton === "undefined") options.singleton = isOldIE();

	// By default, add <style> tags to the bottom of <head>.
	if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

	var styles = listToStyles(list);
	addStylesToDom(styles, options);

	return function update(newList) {
		var mayRemove = [];
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			domStyle.refs--;
			mayRemove.push(domStyle);
		}
		if(newList) {
			var newStyles = listToStyles(newList);
			addStylesToDom(newStyles, options);
		}
		for(var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];
			if(domStyle.refs === 0) {
				for(var j = 0; j < domStyle.parts.length; j++)
					domStyle.parts[j]();
				delete stylesInDom[domStyle.id];
			}
		}
	};
}

function addStylesToDom(styles, options) {
	for(var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];
		if(domStyle) {
			domStyle.refs++;
			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}
			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];
			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}
			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles(list) {
	var styles = [];
	var newStyles = {};
	for(var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};
		if(!newStyles[id])
			styles.push(newStyles[id] = {id: id, parts: [part]});
		else
			newStyles[id].parts.push(part);
	}
	return styles;
}

function insertStyleElement(options, styleElement) {
	var head = getHeadElement();
	var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
	if (options.insertAt === "top") {
		if(!lastStyleElementInsertedAtTop) {
			head.insertBefore(styleElement, head.firstChild);
		} else if(lastStyleElementInsertedAtTop.nextSibling) {
			head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			head.appendChild(styleElement);
		}
		styleElementsInsertedAtTop.push(styleElement);
	} else if (options.insertAt === "bottom") {
		head.appendChild(styleElement);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement(styleElement) {
	styleElement.parentNode.removeChild(styleElement);
	var idx = styleElementsInsertedAtTop.indexOf(styleElement);
	if(idx >= 0) {
		styleElementsInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement(options) {
	var styleElement = document.createElement("style");
	styleElement.type = "text/css";
	insertStyleElement(options, styleElement);
	return styleElement;
}

function createLinkElement(options) {
	var linkElement = document.createElement("link");
	linkElement.rel = "stylesheet";
	insertStyleElement(options, linkElement);
	return linkElement;
}

function addStyle(obj, options) {
	var styleElement, update, remove;

	if (options.singleton) {
		var styleIndex = singletonCounter++;
		styleElement = singletonElement || (singletonElement = createStyleElement(options));
		update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
		remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
	} else if(obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function") {
		styleElement = createLinkElement(options);
		update = updateLink.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
			if(styleElement.href)
				URL.revokeObjectURL(styleElement.href);
		};
	} else {
		styleElement = createStyleElement(options);
		update = applyToTag.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
		};
	}

	update(obj);

	return function updateStyle(newObj) {
		if(newObj) {
			if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
				return;
			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;
		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag(styleElement, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (styleElement.styleSheet) {
		styleElement.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = styleElement.childNodes;
		if (childNodes[index]) styleElement.removeChild(childNodes[index]);
		if (childNodes.length) {
			styleElement.insertBefore(cssNode, childNodes[index]);
		} else {
			styleElement.appendChild(cssNode);
		}
	}
}

function applyToTag(styleElement, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		styleElement.setAttribute("media", media)
	}

	if(styleElement.styleSheet) {
		styleElement.styleSheet.cssText = css;
	} else {
		while(styleElement.firstChild) {
			styleElement.removeChild(styleElement.firstChild);
		}
		styleElement.appendChild(document.createTextNode(css));
	}
}

function updateLink(linkElement, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	if(sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = linkElement.href;

	linkElement.href = URL.createObjectURL(blob);

	if(oldSrc)
		URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/styles.css");


/***/ })

},[2]);
//# sourceMappingURL=styles.bundle.js.map