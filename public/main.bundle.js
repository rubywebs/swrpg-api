webpackJsonp(["main"],{

/***/ "../../../../../src lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src lazy recursive";

/***/ }),

/***/ "../../../../../src/app/STARWARS.eot?":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "STARWARS.fa7a31d20cbd5f199216.eot";

/***/ }),

/***/ "../../../../../src/app/STARWARS.svg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "STARWARS.a4f6233fc6027334b050.svg";

/***/ }),

/***/ "../../../../../src/app/STARWARS.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "STARWARS.3f085be9c96d464c8ed2.ttf";

/***/ }),

/***/ "../../../../../src/app/STARWARS.woff":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "STARWARS.35f31c7aa7cc82eaec5d.woff";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__player_player_component__ = __webpack_require__("../../../../../src/app/player/player.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__player_player_login_player_login_component__ = __webpack_require__("../../../../../src/app/player/player-login/player-login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__player_pc_create_pc_create_component__ = __webpack_require__("../../../../../src/app/player/pc-create/pc-create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_component__ = __webpack_require__("../../../../../src/app/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__player_pc_view_pc_view_component__ = __webpack_require__("../../../../../src/app/player/pc-view/pc-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__guards_player_auth_guard__ = __webpack_require__("../../../../../src/app/guards/player-auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__gm_gm_component__ = __webpack_require__("../../../../../src/app/gm/gm.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__gm_gm_login_gm_login_component__ = __webpack_require__("../../../../../src/app/gm/gm-login/gm-login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__gm_gm_view_gm_view_component__ = __webpack_require__("../../../../../src/app/gm/gm-view/gm-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__guards_gm_auth_guard__ = __webpack_require__("../../../../../src/app/guards/gm-auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












//import { DashboardComponent } from './dashboard.component';
//import { HeroesComponent } from './heroes.component';
//import { HeroDetailComponent } from './hero-detail.component';
var routes = [
    //{ path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_5__home_component__["a" /* HomeComponent */] },
    { path: 'player', component: __WEBPACK_IMPORTED_MODULE_2__player_player_component__["a" /* PlayerComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_7__guards_player_auth_guard__["a" /* PlayerAuthGuard */]] },
    { path: 'player-login', component: __WEBPACK_IMPORTED_MODULE_3__player_player_login_player_login_component__["a" /* PlayerLoginComponent */] },
    { path: 'player/pc-create', component: __WEBPACK_IMPORTED_MODULE_4__player_pc_create_pc_create_component__["a" /* PcCreateComponent */] },
    { path: 'player/pc-view', component: __WEBPACK_IMPORTED_MODULE_6__player_pc_view_pc_view_component__["a" /* PcViewComponent */] },
    { path: 'gm', component: __WEBPACK_IMPORTED_MODULE_8__gm_gm_component__["a" /* GMComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__guards_gm_auth_guard__["a" /* GMAuthGuard */]] },
    { path: 'gm-login', component: __WEBPACK_IMPORTED_MODULE_9__gm_gm_login_gm_login_component__["a" /* GMLoginComponent */] },
    { path: 'gm/gm-view', component: __WEBPACK_IMPORTED_MODULE_10__gm_gm_view_gm_view_component__["a" /* GMViewComponent */] },
    //{ path: 'dashboard',  component: DashboardComponent },
    //{ path: 'detail/:id', component: HeroDetailComponent },
    //{ path: 'heroes',     component: HeroesComponent }
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* @font-face kit by Fonts2u (http://www.fonts2u.com) */ @font-face {font-family:\"STARWARS\";src:url(" + __webpack_require__("../../../../../src/app/STARWARS.eot?") + ") format(\"eot\"),url(" + __webpack_require__("../../../../../src/app/STARWARS.woff") + ") format(\"woff\"),url(" + __webpack_require__("../../../../../src/app/STARWARS.ttf") + ") format(\"truetype\"),url(" + __webpack_require__("../../../../../src/app/STARWARS.svg") + "#STARWARS) format(\"svg\");font-weight:normal;font-style:normal;}\n\n\nbody\n{\n\tbackground-color: #FECF89;\n\tbackground-image: url(\"/assets/tatooine_new.png\");\n\tbackground-size: 100%;\n\tbackground-repeat: no-repeat;\n\tcolor: white;\n}\n\nnav\n{\n    width: 300px;\n    margin-left: auto;\n    margin-right: auto;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Tour of Heroes';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'my-app',
        template: '<router-outlet></router-outlet>'
        //<router-outlet></router-outlet>'
        //templateUrl: 'app.component.html'
        //template: '<h1>HI</h1>'
        //`
        //    <h1>{{title}}</h1>
        //    <nav>
        //        <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
        //        <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
        //    </nav>
        //    <router-outlet></router-outlet>
        //    `,
        ,
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__guards_player_auth_guard__ = __webpack_require__("../../../../../src/app/guards/player-auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__guards_gm_auth_guard__ = __webpack_require__("../../../../../src/app/guards/gm-auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__home_component__ = __webpack_require__("../../../../../src/app/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__player_player_component__ = __webpack_require__("../../../../../src/app/player/player.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__player_player_login_player_login_component__ = __webpack_require__("../../../../../src/app/player/player-login/player-login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__player_pc_select_pc_select_component__ = __webpack_require__("../../../../../src/app/player/pc-select/pc-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__player_pc_create_pc_create_component__ = __webpack_require__("../../../../../src/app/player/pc-create/pc-create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__player_pc_view_pc_view_component__ = __webpack_require__("../../../../../src/app/player/pc-view/pc-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__gm_gm_component__ = __webpack_require__("../../../../../src/app/gm/gm.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__gm_gm_login_gm_login_component__ = __webpack_require__("../../../../../src/app/gm/gm-login/gm-login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__gm_gm_view_gm_view_component__ = __webpack_require__("../../../../../src/app/gm/gm-view/gm-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__gm_gm_view_gm_edit_pc_gm_edit_pc_component__ = __webpack_require__("../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_player_authentication_service__ = __webpack_require__("../../../../../src/app/services/player-authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_gm_authentication_service__ = __webpack_require__("../../../../../src/app/services/gm-authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__gm_gm_view_gm_view_service__ = __webpack_require__("../../../../../src/app/gm/gm-view/gm-view.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__player_pc_select_pc_select_service__ = __webpack_require__("../../../../../src/app/player/pc-select/pc-select.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__player_pc_create_pc_create_service__ = __webpack_require__("../../../../../src/app/player/pc-create/pc-create.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__player_pc_view_pc_view_service__ = __webpack_require__("../../../../../src/app/player/pc-view/pc-view.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























//import { HeroService } from './hero.service';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot()
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_10__player_player_component__["a" /* PlayerComponent */],
            __WEBPACK_IMPORTED_MODULE_11__player_player_login_player_login_component__["a" /* PlayerLoginComponent */],
            __WEBPACK_IMPORTED_MODULE_12__player_pc_select_pc_select_component__["a" /* PcSelectComponent */],
            __WEBPACK_IMPORTED_MODULE_13__player_pc_create_pc_create_component__["a" /* PcCreateComponent */],
            __WEBPACK_IMPORTED_MODULE_14__player_pc_view_pc_view_component__["a" /* PcViewComponent */],
            __WEBPACK_IMPORTED_MODULE_15__gm_gm_component__["a" /* GMComponent */],
            __WEBPACK_IMPORTED_MODULE_16__gm_gm_login_gm_login_component__["a" /* GMLoginComponent */],
            __WEBPACK_IMPORTED_MODULE_17__gm_gm_view_gm_view_component__["a" /* GMViewComponent */],
            __WEBPACK_IMPORTED_MODULE_18__gm_gm_view_gm_edit_pc_gm_edit_pc_component__["a" /* GMEditPcComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_7__guards_player_auth_guard__["a" /* PlayerAuthGuard */],
            __WEBPACK_IMPORTED_MODULE_19__services_player_authentication_service__["a" /* PlayerAuthenticationService */],
            __WEBPACK_IMPORTED_MODULE_22__player_pc_select_pc_select_service__["a" /* PcSelectService */],
            __WEBPACK_IMPORTED_MODULE_23__player_pc_create_pc_create_service__["a" /* PcCreateService */],
            __WEBPACK_IMPORTED_MODULE_24__player_pc_view_pc_view_service__["a" /* PcViewService */],
            __WEBPACK_IMPORTED_MODULE_8__guards_gm_auth_guard__["a" /* GMAuthGuard */],
            __WEBPACK_IMPORTED_MODULE_20__services_gm_authentication_service__["a" /* GMAuthenticationService */],
            __WEBPACK_IMPORTED_MODULE_21__gm_gm_view_gm_view_service__["a" /* GMViewService */],
            __WEBPACK_IMPORTED_MODULE_25__services_server_url_service__["a" /* ServerUrlService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/gm/gm-login/gm-login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form\n{\n    width: 300px;\n    margin-left: auto;\n    margin-right: auto;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/gm/gm-login/gm-login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"gm_signin\" ng-hide=\"signed_in\">\n    <h2 class=\"text-center\">Please sign in</h2>\n    <h3 *ngIf=\"error\">{{error}}</h3>\n\t<form id=\"gm-sign-in\">\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"email\">Email</label>\n\t\t\t<input class=\"form-control\" type=\"text\" name=\"email\" [(ngModel)]=\"gm.email\" placeholder=\"GM Email\">\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"password\">Password</label>\n\t\t\t<input class=\"form-control\" type=\"password\" name=\"password\" [(ngModel)]=\"gm.password\" placeholder=\"Password\">\n\t\t</div>\n\t\t<button type=\"submit\" class=\"btn btn-default\" (click)=\"login()\">Login</button>\n\t</form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/gm/gm-login/gm-login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMLoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_gm_authentication_service__ = __webpack_require__("../../../../../src/app/services/gm-authentication.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GMLoginComponent = (function () {
    function GMLoginComponent(router, gmAuthService) {
        this.router = router;
        this.gmAuthService = gmAuthService;
        this.error = '';
        this.gm = {};
    }
    GMLoginComponent.prototype.ngOnInit = function () {
    };
    GMLoginComponent.prototype.login = function () {
        var _this = this;
        this.gmAuthService.login(this.gm.email, this.gm.password)
            .then(function (gm1) {
            console.log(gm1);
            localStorage.setItem('currentGM', JSON.stringify({ id: gm1.user_id, name: gm1.name, email: gm1.email, token: gm1.auth_token }));
            console.log(localStorage.getItem('currentGM'));
            _this.router.navigate(['/gm/gm-view']);
            //this.playerComponent.currentPlayer = player;
        })
            .catch(function (response) {
            _this.error = "Error logging in!";
        });
    };
    return GMLoginComponent;
}());
GMLoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        template: __webpack_require__("../../../../../src/app/gm/gm-login/gm-login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/gm/gm-login/gm-login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_gm_authentication_service__["a" /* GMAuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_gm_authentication_service__["a" /* GMAuthenticationService */]) === "function" && _b || Object])
], GMLoginComponent);

var _a, _b;
//# sourceMappingURL=gm-login.component.js.map

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div id=\"gm_pcs\" ng-show=\"gmViewStage === 'gmEditPc'\">\n    <!--Edit PC page \n    <div class=\"row\" id=\"edit_pc\">\n        <div class=\"col-md-12\">\n            <ul id=\"gm_pc_update\">\n                <li><a href ng-click=\"delete_pc(character.id)\">DELETE PC</a></li>\n                <li><a href ng-click=\"gm_modify_pc()\">SAVE UPDATES</a></li>\n                <li><a href ng-click=\"close_edit_pc_state()\">CANCEL</a></li>        \n                <li>Changed Skills: {{changed_skills}}</li>\n            </ul>\n        </div>\n\n        <div class=\"col-md-6\">\n            <form class=\"form\">\n                <div class=\"form-group\">\n                    <label>Character Name</label>\n                    <input class=\"form-control\" type=\"text\" ng-model=\"character.name\">\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Species</label>\n                    <select class=\"form-control\" ng-model=\"character.race\" ng-options=\"race.name for race in races track by race.id\"></select>\n                </div>\n                \n                <div class=\"form-group\">\n                    <label>Career</label>\n                    <select class=\"form-control\" ng-change=\"careerSelected()\" ng-model=\"character.career\" ng-options=\"career.name for career in careers track by career.id\"></select>\n                </div>\n            \n                <div class=\"form-group\">\n                    <label>Specializations</label>\n                    <div ng-repeat=\"spec in character.specialization\">\n                        <p>{{spec.name}}\n                        <button class=\"button btn-remove\">--</button></p>\n                    </div>\n                </div>\n                \n                <table id=\"pc_attributes\" class=\"table table-condensed\">\n                    <thead>\n                        <tr>\n                            <td><label>Attribute</label></td>\n                            <td class=\"table-rank\"><label>Rank</label></td>\n                            <td><label>Attribute</label></td>\n                            <td class=\"table-rank\"><label>Rank</label></td>\n                            <td><label>Attribute</label></td>\n                            <td class=\"table-rank\"><label>Rank</label></td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td>Agility</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.agility\"></td>\n                            <td>Brawn</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.brawn\"></td>\n                            <td>Cunning</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.cunning\"></td>\n                        </tr>\n                        <tr>\n                            <td>Intellect</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.intellect\"></td>\n                            <td>Presence</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.presence\"></td>\n                            <td>Willpower</td>\n                            <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.willpower\"></td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <table id=\"pc_health\" class=\"table table-condensed\">\n                <thead>\n                    <tr>\n                        <td><label>Current Wounds</label></td>\n                        <td><label>Max Wounds</label></td>\n                        <td><label>Current Strain</label></td>\n                        <td><label>Max Strain</label></td>\n                        <td><label>XP</label></td>\n                        <td><label>Credits</label></td>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr>\n                        <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.wounds_current\"></td>\n                        <td class=\"table-rank\">{{character.wounds_thresh}}</td>\n                        <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.strain_current\"></td>\n                        <td class=\"table-rank\">{{character.strain_thresh}}</td>\n                        <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.xp\"></td>\n                        <td class=\"table-rank\"><input class=\"form-control\" ng-model=\"character.credits\"></td>\n                    </tr>\n                </tbody>\n                </table>\n            </form>   \n        \n                \n           <table class=\"table table-condensed\">\n               <caption><img class=\"circle-add-sm\" ng-click=\"open_add_weapon_pc_dialog()\" ng-src=\"/images/circle_add.png\">&nbsp Weapons</caption>\n               <tr>\n                   <th></th>\n                   <th>Name</th>\n                   <th>Description</th>\n               </tr>\n               <tr ng-repeat=\"w in character.weapons\">\n                   <td><img class=\"circle-delete-sm\" ng-click=\"delete_weapon_from_pc($index, w.id)\" ng-src=\"/images/circle_delete.png\"></td>\n                   <td>{{w.name}}</td>\n                   <td>{{w.description}}</td>\n               </tr>\n            </table>\n            \n            <table class=\"table table-condensed\">\n                <caption><img class=\"circle-add-sm\" ng-click=\"open_add_armor_pc_dialog()\" ng-src=\"/images/circle_add.png\">&nbsp Armor</caption>\n                <tr>\n                    <th></th>\n                    <th>Name</th>\n                    <th>Description</th>\n                </tr>\n                <tr ng-repeat=\"a in character.armors\">\n                    <td><img class=\"circle-delete-sm\" ng-click=\"delete_armor_from_pc($index, a.id)\" ng-src=\"/images/circle_delete.png\"></td>\n                    <td>{{a.name}}</td>\n                    <td>{{a.description}}</td>\n                </tr>\n            </table>\n            \n            <table class=\"table table-condense\">\n                <caption><img class=\"circle-add-sm\" ng-click=\"open_add_item_pc_dialog()\" ng-src=\"/images/circle_add.png\">&nbsp Items</caption>\n                <tr>\n                    <th></th>\n                    <th>Name</th>\n                    <th>Description</th>\n                </tr>\n                <tr ng-repeat=\"i in character.items\">\n                    <td><img class=\"circle-delete-sm\" ng-click=\"delete_item_from_pc($index, i.id)\" ng-src=\"/images/circle_delete.png\"></td>\n                    <td>{{i.name}}</td>\n                    <td>{{i.description}}</td>\n                </tr>\n            </table>\n        </div>\n        \n        <div class=\"col-md-6\">\n            <table class=\"table table-condensed\">\n                <thead>\n                    <tr>\n                        <td>Name</td>\n                        <td>Attribute</td>\n                        <td>Career</td>\n                        <td id=\"skill-rank\">Rank</td>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr ng-repeat=\"skill in skills\" >\n                        <td><label>{{skill.name}}</label></td>\n                        <td><label>{{skill.attrib}}</label></td>\n                            <td ng-if=\"skill.career == true\">*</td>\n                            <td ng-if=\"skill.career == false\"></td>\n                        <td class=\"rank\"><input ng-change=\"skill_rank_changed({{skill}}, skill.rank)\" class=\"form-control\" ng-model=skill.rank></td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMEditPcComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GMEditPcComponent = (function () {
    function GMEditPcComponent() {
    }
    GMEditPcComponent.prototype.ngOnInit = function () { };
    return GMEditPcComponent;
}());
GMEditPcComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'gm-edit-pc',
        template: __webpack_require__("../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.html"),
        styles: [__webpack_require__("../../../../../src/app/gm/gm-view/gm-edit-pc/gm-edit-pc.component.css")]
    })
], GMEditPcComponent);

//# sourceMappingURL=gm-edit-pc.component.js.map

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"gm_signed_in\">\n\t<div class=\"row\">\n        <nav class=\"col-lg-2\">\n            <ul id=\"admin_nav\">\n                <li><img class=\"nav-header\" src=\"/assets/nav/admin_setup.png\"></li>\n                <li>\n                    <a (click)=\"gm_set_stage('gmPcs')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/player_characters.png\">\n                    </a>\n                </li>\n                <li>\n                    <a href (click)=\"gm_set_stage('gm_npcs')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/npcs.png\">\n                    </a>\n                </li>\n                <li>\n                    <a href>\n                    <img class=\"nav-item\" src=\"/assets/nav/equipment.png\">\n                    </a>\n                </li>\n                <li>\n                    <a href (click)=\"gm_set_stage('gm_campaign_setup')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/campaign_setup.png\">\n                    </a>\n                </li>\n\n                <li><img class=\"nav-header\" src=\"/assets/nav/campaign.png\"></li>\n\n                <li>\n                    <a href (click)=\"gm_set_stage('gm_campaign_select')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/campaign_select.png\">\n                    </a>\n                </li>\n\n                <div *ngIf=\"current_session === ''\">\n                <li class=\"game_session\">{{current_session.name}}</li>\n                <li>\n                    <a href (click)=\"gm_set_stage('gm_campaign_overview')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/campaign_overview.png\">\n                    </a>\n                </li>\n\n                <li>\n                    <a href (click)=\"gm_set_stage('gm_campaign_players')\">\n                    <img class=\"nav-item\" src=\"/assets/nav/campaign_pcs.png\">\n                    </a>\n                </li>\n                </div>\n            </ul>\n        </nav>\n\n        <section id=\"gm-content\" class=\"col-lg-10\">\n            <h3>CODE URL TEMP</h3>\n            <form>\n                <input class=\"form-control\" name=\"url\" [(ngModel)]=\"url\" type=\"text\"/>\n                <button (click)=\"add_url()\">ADD URL</button>\n            </form>\n\n        \t<div id=\"gm_overview\" *ngIf=\"gmViewStage === 'gmPcs'\">\n                <div class=\"row\">\n            \t\t<div class=\"gm-all-pcs\" *ngFor=\"let pc of allPcs\">\n                        <p>\n                            Name: {{pc.name}}<br>\n                            Race: {{pc.race_name}}<br>\n                            Career: {{pc.career_name}}<br>\n                            XP: {{pc.xp}}<br>\n                            Credit: {{pc.credits}}<br>\n                            Status: {{pc.status}}\n                        </p>\n                        <a (click)=\"edit_pc($index)\">Edit</a>\n        \t\t\t</div>\n            \t</div>\n        \t</div>\n        \t<gm-edit-pc *ngIf=\"gmViewStage === 'gmEditPc'\"></gm-edit-pc>\n        </section>\n   </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gm_view_service__ = __webpack_require__("../../../../../src/app/gm/gm-view/gm-view.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GMViewComponent = (function () {
    function GMViewComponent(GMViewService, router) {
        this.GMViewService = GMViewService;
        this.router = router;
        this.gmViewStage = "overview"; //What stage is the view on?
        this.allPcs = [];
    }
    GMViewComponent.prototype.ngOnInit = function () {
    };
    GMViewComponent.prototype.add_url = function () {
        this.GMViewService.addUrl(this.url);
        this.url = "";
    };
    GMViewComponent.prototype.gm_set_stage = function (stage) {
        var _this = this;
        this.gmViewStage = stage;
        switch (this.gmViewStage) {
            case "gmPcs":
                this.GMViewService.getAllPcs()
                    .then(function (res) {
                    _this.allPcs = _this.GMViewService.allPcs;
                    console.log(_this.allPcs);
                });
                //this.allPcs = this.GMViewService.allPcs;
                //console.log(this.allPcs);
                break;
            default:
                // code...
                break;
        }
    };
    GMViewComponent.prototype.edit_pc = function (index) {
        this.gm_set_stage("gmEditPc");
        this.character = this.allPcs[index];
    };
    return GMViewComponent;
}());
GMViewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        template: __webpack_require__("../../../../../src/app/gm/gm-view/gm-view.component.html"),
        styles: [__webpack_require__("../../../../../src/app/gm/gm-view/gm-view.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__gm_view_service__["a" /* GMViewService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__gm_view_service__["a" /* GMViewService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], GMViewComponent);

var _a, _b;
//# sourceMappingURL=gm-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/gm/gm-view/gm-view.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMViewService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GMViewService = (function () {
    function GMViewService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.currentGM = JSON.parse(localStorage.getItem('currentGM'));
        this.authHeader = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.authHeader.append('Authorization', " " + this.currentGM.token);
    }
    //TEMP MOVE TO DIFFERENT WEBSITE
    //ADD URL TO CODE AD SERVER
    GMViewService.prototype.addUrl = function (url) {
        return this.http.post(this.serverUrl.baseUrl + 'add_url.json', { url: url })
            .toPromise()
            .then(function (res) { return console.log(res); });
    };
    GMViewService.prototype.getAllPcs = function () {
        var _this = this;
        //Clear out allPcs
        this.allPcs = [];
        return this.http.post(this.serverUrl.baseUrl + '/gm/get-all-pcs.json', { headers: this.authHeader, id: this.currentGM.id }, { headers: this.authHeader }) //JSON.stringify({id: this.currentUser.id, auth_token: this.currentUser.token}), {headers: this.authHeader})
            .toPromise()
            .then(function (res) {
            _this.allPcs = res.json();
        })
            .catch(this.handleError);
    };
    GMViewService.prototype.handleError = function (error) {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return GMViewService;
}());
GMViewService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], GMViewService);

var _a, _b;
//# sourceMappingURL=gm-view.service.js.map

/***/ }),

/***/ "../../../../../src/app/gm/gm.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/gm/gm.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/gm/gm.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_gm__ = __webpack_require__("../../../../../src/app/models/gm.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_gm_authentication_service__ = __webpack_require__("../../../../../src/app/services/gm-authentication.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { Pc } from '../models/pc';
//import { PcSelectComponent } from './pc-select/pc-select.component';
//import { PcSelectService } from './pc-select/pc-select.service';
//import { PlayerAuthenticationService } from '../services/player-authentication.service';

var GMComponent = (function () {
    //selectPc: Pc;
    function GMComponent(router, GMAuthService) {
        this.router = router;
        this.GMAuthService = GMAuthService;
        this.currentGM = new __WEBPACK_IMPORTED_MODULE_2__models_gm__["a" /* GM */];
        var c_GM = JSON.parse(localStorage.getItem('currentGM'));
        this.currentGM.user_id = c_GM['id'];
        this.currentGM.name = c_GM['name'];
        //this.currentGM.email = c_GM["email"];
        console.log(this.currentGM);
    }
    GMComponent.prototype.logOut = function () {
        //Log out the current player and return to the login screen
        this.GMAuthService.logout();
        this.router.navigate(['/gm-login']);
    };
    return GMComponent;
}());
GMComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'gm',
        template: __webpack_require__("../../../../../src/app/gm/gm.component.html"),
        styles: [__webpack_require__("../../../../../src/app/gm/gm.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_gm_authentication_service__["a" /* GMAuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_gm_authentication_service__["a" /* GMAuthenticationService */]) === "function" && _b || Object])
], GMComponent);

var _a, _b;
//# sourceMappingURL=gm.component.js.map

/***/ }),

/***/ "../../../../../src/app/guards/gm-auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMAuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
//The auth guard is used to prevent unauthenticated users 
//from accessing restricted routes, it's used in app.routing.ts 
//to protect pages
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GMAuthGuard = (function () {
    function GMAuthGuard(router) {
        this.router = router;
    }
    GMAuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('currentGM')) {
            // logged in so return true
            return true;
        }
        // Not logged in so redirect to player login
        this.router.navigate(['/gm-login']);
        return false;
    };
    return GMAuthGuard;
}());
GMAuthGuard = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object])
], GMAuthGuard);

var _a;
//# sourceMappingURL=gm-auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/guards/player-auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerAuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
//The auth guard is used to prevent unauthenticated users 
//from accessing restricted routes, it's used in app.routing.ts 
//to protect pages
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlayerAuthGuard = (function () {
    function PlayerAuthGuard(router) {
        this.router = router;
    }
    PlayerAuthGuard.prototype.canActivate = function () {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        // Not logged in so redirect to player login
        this.router.navigate(['/player-login']);
        return false;
    };
    return PlayerAuthGuard;
}());
PlayerAuthGuard = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object])
], PlayerAuthGuard);

var _a;
//# sourceMappingURL=player-auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body\n{\n\tbackground-color: #FECF89;\n\tbackground-image: url(\"/assets/tatooine_new.png\");\n\tbackground-size: auto;\n\tbackground-repeat: no-repeat;\n\tcolor: white;\n}\n\nnav\n{\n    width: 300px;\n    margin-left: auto;\n    margin-right: auto;\n}\n\ndiv#enter-small\n{\n\theight: 800px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n\t<div class=\"container-fluid\">\n\t\t<nav>\n\t\t\t<div id=\"enter-small\" class=\"row\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<a routerLink=\"/gm\"><img class=\"main-nav\" src=\"/assets/gm-small.png\"></a>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<img class=\"main-nav\" src=\"/assets/view-small.png\">\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<a routerLink=\"/player\"><img class=\"main-nav\" src=\"/assets/player-small.png\"></a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</nav>\n\t</div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HomeComponent = (function () {
    function HomeComponent() {
        this.title = 'Star Wars: Edge Of the Galaxy';
    }
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'home-app',
        template: __webpack_require__("../../../../../src/app/home.component.html")
        //template: '<h1>HI</h1>'
        //`
        //    <h1>{{title}}</h1>
        //    <nav>
        //        <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
        //        <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
        //    </nav>
        //    <router-outlet></router-outlet>
        //    `,
        ,
        styles: [__webpack_require__("../../../../../src/app/home.component.css")]
    })
], HomeComponent);

//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/models/gm.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GM; });
var GM = (function () {
    function GM() {
    }
    return GM;
}());

//# sourceMappingURL=gm.js.map

/***/ }),

/***/ "../../../../../src/app/models/pc.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Pc; });
var Pc = (function () {
    function Pc() {
    }
    return Pc;
}());

//# sourceMappingURL=pc.js.map

/***/ }),

/***/ "../../../../../src/app/models/player.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Player; });
var Player = (function () {
    function Player() {
    }
    return Player;
}());

//# sourceMappingURL=player.js.map

/***/ }),

/***/ "../../../../../src/app/models/race.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Race; });
var Race = (function () {
    function Race() {
    }
    return Race;
}());

//# sourceMappingURL=race.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-create/pc-create.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body\n{\n    background-color: #F5F5F5;\n}\n\ntable\n{\n    font-size: 10px;\n}\n\n.selection\n{\n    background-color: white;\n    padding: 15px;\n    border-radius: 15px;\n}\n\n.heading\n{\n    color: rgb(169, 69, 68);\n    font-family: 'STARWARS', sans-serif;\n}\n\n.emphasis\n{\n    color: rgb(160, 60, 68);\n    font-weight: bold;\n}\n\n.btn-increase\n{\n\tcolor: green;\n\tborder: solid 1px green;\n\tborder-radius: 10px;\n\tbackground-color: greenyellow;\n\tfloat: right;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/player/pc-create/pc-create.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n<!-- Stage 1 of character creation -->\n<div *ngIf=\"pcCreateStage==='pcStage1'\" class=\"row\">\n    <div class=\"col-md-offset-3 col-md-6\">\n    <h3 class=\"text-center heading\">CREATE A NEW CHARACTER</h3>\n    <h4 class=\"text-center\"><a href routerLink=\"/player\">Cancel New Character</a></h4>\n    \n    <form class=\"selection\">\n        <div class=\"form-group\">\n            <label class=\"emphasis\">CHARACTER NAME</label>\n            <input class=\"form-control\" type=\"text\" [(ngModel)]=\"character.name\" name=\"name\"/>\n        </div>\n        \n        <div class=\"form-group\">\n            <label class=\"emphasis\">SPECIES</label>\n            <select class=\"form-control\" [(ngModel)]=\"character.race_id\" (ngModelChange)='raceChange($event)' name=\"race\">\n                <option *ngFor=\"let race of allRaces\" [value]=\"race.id\">{{race.name}}</option>\n            </select>\n        </div>\n        \n        <div class=\"form-group\">\n            <label class=\"emphasis\">CAREER</label>\n            <select class=\"form-control\" [(ngModel)]=\"character.career_id\" (ngModelChange)='careerChange($event)' name=\"career\">\n                <option *ngFor=\"let career of allCareers\" [value]=\"career.id\">{{career.name}}</option>\n            </select>\n        </div>\n\n        <div *ngIf=\"careerSpecializations\" class=\"form-group\">\n            <label class=\"emphasis\">SPECIALIZATION</label>\n            <select class=\"form-control\" [(ngModel)]=\"character.spec_id\" name=\"specialization\">\n                <option *ngFor=\"let spec of careerSpecializations\" [value]=\"spec.id\">{{spec.name}}</option>\n            </select>\n        </div>\n\n        <button class=\"btn btn-default\" (click)=\"pcStage2()\">Save {{character.name}}</button>\n\n        <div *ngIf=\"raceDetails\">\n        <h3 class=\"text-center heading\">CHARACTER PREVIEW</h3>\n        <p class=\"text-center\">{{raceDetails.description}}</p>\n        \n        <table class=\"table-bordered table table-condensed\" id=\"character-stats-preview\">\n            <tr>\n                <td class=\"emphasis\">Brawn</td>\n                <td>{{raceDetails.brawn}}</td>\n                <td class=\"emphasis\">Agility</td>\n                <td>{{raceDetails.agility}}</td>\n            </tr>\n            \n            <tr>\n                <td class=\"emphasis\">Intellect</td>\n                <td>{{raceDetails.intellect}}</td>\n                <td class=\"emphasis\">Presence</td>\n                <td>{{raceDetails.presence}}</td>\n            </tr>\n            \n            <tr>\n                <td class=\"emphasis\">Willpower</td>\n                <td>{{raceDetails.willpower}}</td>\n                <td class=\"emphasis\">Cunning</td>\n                <td>{{raceDetails.cunning}}</td>\n            </tr>\n        </table>\n        </div>\n    </form>\n    </div>\n    \n    <div *ngIf=\"raceDetails\" class=\"col-md-offset-3 col-md-6\">\n   \t\t<img [src]='raceDetails.avatar' id=\"avatar-preview\">\n    </div>\n</div>\n\n<!-- Stage 2 of character creation -->\n<div *ngIf=\"pcCreateStage === 'pcStage2'\" class=\"row\">\n    <div class=\"col-md-offset-3 col-md-6\">\n    <h3 class=\"text-center heading\">{{newPc.name}}</h3>\n    \n    <h4 class=\"text-center emphasis\">Select 4 skills to increase rank for free</h4>\n    <table id=\"skills\" class=\"table selection\">\n        <thead>\n            <tr>\n                <th id=\"skill\">Skill</th>\n                <th id=\"attribute\">Attribute</th>\n                <th class=\"text-right\" id=\"rank\">Rank</th>\n                <th class=\"text-right\">Increase</th>\n            </tr>\n        </thead>\n        \n        <tbody>\n            <tr *ngFor=\"let skill of newPcCareerSkills\" >\n                <td>{{skill.name}}</td>\n                <td>{{skill.attrib}}</td>\n                <td class=\"text-right\">{{skill.rank}}</td>\n                <td *ngIf=\"careerSkillChoices > 0\"><button class=\"button btn-increase\" (click)=\"increaseCareerSkillRank($index,skill.id,false)\">+</button></td>\n            </tr>\n        </tbody>\n    </table>\n    \n    <h4 class=\"text-center\">You have {{careerSkillChoices}} left</h4>\n    \n    <h4 class=\"text-center emphasis\">Select 2 skills to increase rank for free</h4>\n    <table id=\"career-skills\" class=\"table selection\">\n        <thead>\n            <tr>\n                <th id=\"skill\">Skill</th>\n                <th id=\"attribute\">Attribute</th>\n                <th class=\"text-right\">Increase</th>\n            </tr>\n        </thead>\n        \n        <tbody>\n            <tr *ngFor=\"let skill of newPcCareerSpecializationSkills\" >\n                <td>{{skill.name}}</td>\n                <td>{{skill.attrib}}</td>\n                <td *ngIf=\"specSkillChoices > 0\"><button class=\"button btn-increase\" (click)=\"increaseSpecializationSkillRank($index,skill.id,false)\">+</button></td>\n            </tr>\n        </tbody>\n    </table>\n    \n    <h4 class=\"text-center\">You have {{specSkillChoices}} left</h4>\n    \n    <button class=\"btn\" *ngIf=\"careerSkillChoices === 0 && specSkillChoices === 0\" (click)=\"pcStage3()\">Continue</button>\n    </div>\n</div>\n\n<!-- Stage 3 of character creation -->\n<div *ngIf=\"pcCreateStage === 'pcStage3'\" class=\"row\">\n    <div class=\"col-md-offset-3 col-md-6\">\n    <table id=\"attributes\" class=\"table\">\n        <thead>\n            <tr>\n                <th>Attribute</th>\n                <th>Score</th>\n                <th>Cost</th>\n                <th class=\"text-right\">Increase</th>\n            </tr>\n        </thead>    \n        \n        <tbody>\n            <tr>\n                <td>Agility</td>\n                <td>{{newPc.agility}}</td>\n                <td>{{(newPc.agility + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('agility')\" *ngIf=\"newPc.xp >= (newPc.agility + 1) * 10 && newPc.agility < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n            <tr>\n                <td>Brawn</td>\n                <td>{{newPc.brawn}}</td>\n                <td>{{(newPc.brawn + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('brawn')\" *ngIf=\"newPc.xp >= (newPc.brawn + 1) * 10 && newPc.brawn < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n            <tr>\n                <td>Cunning</td>\n                <td>{{newPc.cunning}}</td>\n                <td>{{(newPc.cunning + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('cunning')\" *ngIf=\"newPc.xp >= (newPc.cunning + 1) * 10 && newPc.cunning < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n            <tr>\n                <td>Intellect</td>\n                <td>{{newPc.intellect}}</td>\n                <td>{{(newPc.intellect + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('intellect')\" *ngIf=\"newPc.xp >= (newPc.intellect + 1) * 10 && newPc.intellect < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n            <tr>\n                <td>Presence</td>\n                <td>{{newPc.presence}}</td>\n                <td>{{(newPc.presence + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('presence')\" *ngIf=\"newPc.xp >= (newPc.presence + 1) * 10 && newPc.presence < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n            <tr>\n                <td>Willpower</td>\n                <td>{{newPc.willpower}}</td>\n                <td>{{(newPc.willpower + 1) * 10}}</td>\n                <td><button (click)=\"increaseAttribute('willpower')\" *ngIf=\"newPc.xp >= (newPc.willpower + 1) * 10 && newPc.willpower < 5\" class=\"button btn-increase\">+</button></td>\n            </tr>\n        </tbody>\n    </table>\n    \n    <table id=\"skills\" class=\"table\">\n        <thead>\n            <tr>\n                <th id=\"skill\">Skill</th>\n                <th id=\"attribute\">Attribute</th>\n                <th id=\"rank\">Rank</th>\n                <th id=\"career\">Career</th>\n                <th id=\"cost\">Cost</th>\n                <th class=\"text-right\">Increase</th>\n            </tr>\n        </thead>\n        \n        <tbody>\n            <tr *ngFor=\"let skill of newPc.skills\" >\n                <td>{{skill.name}}</td>\n                <td>{{skill.attrib}}</td>\n                <td class=\"text-right\">{{skill.rank}}</td>\n                    <td *ngIf=\"skill.career == true\">*</td>\n                    <td *ngIf=\"skill.career == false\"></td>\n                    <td *ngIf=\"skill.career == true\">{{(skill.rank + 1) * 5}}</td>\n                    <td *ngIf=\"skill.career == false\">{{((skill.rank +1) * 5) + 5}}</td>\n                    <td *ngIf=\"skill.career == true\"><button (click)=\"increaseSkillRank(skill.id, true)\" *ngIf=\"newPc.xp >= (skill.rank + 1) * 5 && skill.rank < 2\" class=\"button btn-increase\">+</button></td>\n                    <td *ngIf=\"skill.career == false\"><button (click)=\"increaseSkillRank(skill.id, true)\" *ngIf=\"newPc.xp >= ((skill.rank + 1) * 5) + 5 && skill.rank < 2\" class=\"button btn-increase\">+</button></td>\n            </tr>\n        </tbody>\n    </table>\n    <h5 class=\"emphasize text-center\">Remaining XP {{newPc.xp}}</h5>\n    <button class=\"btn\" (click)=\"pcFinalize()\">Finish</button>\n</div>\n</div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/player/pc-create/pc-create.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcCreateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_pc__ = __webpack_require__("../../../../../src/app/models/pc.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_race__ = __webpack_require__("../../../../../src/app/models/race.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pc_create_service__ = __webpack_require__("../../../../../src/app/player/pc-create/pc-create.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PcCreateComponent = (function () {
    function PcCreateComponent(pcCreateService, router) {
        this.pcCreateService = pcCreateService;
        this.router = router;
    }
    PcCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        //New PC
        this.character = new __WEBPACK_IMPORTED_MODULE_2__models_pc__["a" /* Pc */]();
        //Set initial pcCreateStage to pcStage1
        this.pcCreateStage = 'pcStage1';
        //Set skill choice counts
        this.careerSkillChoices = 4;
        this.specSkillChoices = 2;
        //Get the list of races
        this.pcCreateService.getRaces()
            .then(function (res) {
            //Save the response to allRaces
            _this.allRaces = res;
        })
            .catch(function (res) { return console.log("Error getting races"); });
        this.pcCreateService.getCareers()
            .then(function (res) {
            //Save the response to allCareers
            _this.allCareers = res;
        })
            .catch(function (res) { return console.log("Error getting careers"); });
    };
    //Called when race select box changes
    //Updates race details
    PcCreateComponent.prototype.raceChange = function (race_id) {
        var _this = this;
        this.allRaces.forEach(function (element) {
            if (element.id === +race_id) {
                //If the element.id (from allRaces) = race_id (from selection) save as details for view
                _this.raceDetails = new __WEBPACK_IMPORTED_MODULE_3__models_race__["a" /* Race */]();
                _this.raceDetails = element;
            }
        });
    };
    //Called when career select box changes
    //Updates specializations
    PcCreateComponent.prototype.careerChange = function (career_id) {
        var _this = this;
        this.allCareers.forEach(function (element) {
            if (element.id === +career_id) {
                //If the element.id (from allCareers) = career_id
                _this.pcCreateService.getCareerSpecializations(career_id)
                    .then(function (res) {
                    _this.careerSpecializations = res;
                })
                    .catch(function () { return console.log("Error getting specializations"); });
            }
        });
    };
    PcCreateComponent.prototype.increaseCareerSkillRank = function (index, skillId, useXp) {
        var _this = this;
        this.pcCreateService.increaseSkillRank(this.newPc.id, skillId, false)
            .then(function () {
            //Increase the skill rank
            _this.newPc.skills.forEach(function (skill) {
                if (skill.id == +skillId) {
                    skill.rank = skill.rank + 1;
                }
            });
            //Decrease availble count and remove from list
            _this.careerSkillChoices = _this.careerSkillChoices - 1;
            _this.newPcCareerSkills.splice(index, 1);
        })
            .catch(function () { return console.log("Error increasing skill rank"); });
    };
    PcCreateComponent.prototype.increaseSpecializationSkillRank = function (index, skillId, useXp) {
        var _this = this;
        this.pcCreateService.increaseSkillRank(this.newPc.id, skillId, false)
            .then(function () {
            //Decrease availble count and remove from list
            _this.specSkillChoices = _this.specSkillChoices - 1;
            _this.newPcCareerSpecializationSkills.splice(index, 1);
        })
            .catch(function () { return console.log("Error increasing skill rank"); });
    };
    PcCreateComponent.prototype.increaseSkillRank = function (skillId, useXp) {
        var _this = this;
        this.pcCreateService.increaseSkillRank(this.newPc.id, skillId, useXp)
            .then(function () {
            //Increase the skill rank
            _this.newPc.skills.forEach(function (skill) {
                if (skill.id == +skillId) {
                    skill.rank = skill.rank + 1;
                }
            });
            //Get updated xp
            _this.pcCreateService.getPcXp(_this.newPc.id)
                .then(function (res) { return _this.newPc.xp = res.xp; })
                .catch(function () { return console.log("Error getting updated XP"); });
        });
    };
    PcCreateComponent.prototype.increaseAttribute = function (attribute) {
        var _this = this;
        this.pcCreateService.increaseAttribute(this.newPc.id, attribute)
            .then(function () {
            switch (attribute) {
                case 'agility':
                    _this.newPc.agility++;
                    break;
                case 'brawn':
                    _this.newPc.brawn++;
                    break;
                case 'cunning':
                    _this.newPc.cunning++;
                    break;
                case 'intellect':
                    _this.newPc.intellect++;
                    break;
                case 'presence':
                    _this.newPc.presence++;
                    break;
                case 'willpower':
                    _this.newPc.willpower++;
                    break;
                default:
                    break;
            }
            //get updated xp
            _this.pcCreateService.getPcXp(_this.newPc.id)
                .then(function (res) { return _this.newPc.xp = res.xp; })
                .catch(function () { return console.log("Error getting updated XP"); });
        })
            .catch(function () { return console.log("Error increasing attribute"); });
    };
    //Move to stage 2 of Pc creation
    PcCreateComponent.prototype.pcStage2 = function () {
        var _this = this;
        //Create a new Pc and save the base Pc to work with
        this.pcCreateService.createNewPc(this.character.name, this.character.race_id, this.character.career_id)
            .then(function (res) {
            //Pc created on server.  Save the PC for further updating
            _this.newPc = res;
            console.log(res);
            _this.newPcCareerSkills = [];
            _this.newPcCareerSpecializationSkills = [];
            //Save Career Skills for skill setup
            _this.newPc.skills.forEach(function (skill) {
                if (skill.career)
                    _this.newPcCareerSkills.push(skill);
            });
            //Assign initial specialization.  Use no XP (false)
            _this.pcCreateService.addNewPcSpecialization(_this.newPc.id, _this.character.spec_id, false)
                .then(function () {
                //Get the specializaion skills available and save for view
                _this.pcCreateService.getCareerSpecializationSkills(_this.character.spec_id)
                    .then(function (res) { return _this.newPcCareerSpecializationSkills = res; })
                    .catch(function (res) { return console.log(res); });
                _this.pcCreateStage = 'pcStage2';
            });
        })
            .catch(function (res) { return console.log(res); });
    };
    PcCreateComponent.prototype.pcStage3 = function () {
        //Time to use initial XP
        this.pcCreateStage = 'pcStage3';
    };
    PcCreateComponent.prototype.pcFinalize = function () {
        var _this = this;
        //Finalize the PC.  Set status to 'active'
        this.pcCreateService.finalizeNewPc(this.newPc.id)
            .then(function () {
            //Navigate back to PC select
            _this.router.navigateByUrl('/player');
        });
    };
    return PcCreateComponent;
}());
PcCreateComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'pc-create',
        template: __webpack_require__("../../../../../src/app/player/pc-create/pc-create.component.html"),
        styles: [__webpack_require__("../../../../../src/app/player/pc-create/pc-create.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__pc_create_service__["a" /* PcCreateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__pc_create_service__["a" /* PcCreateService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], PcCreateComponent);

var _a, _b;
//# sourceMappingURL=pc-create.component.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-create/pc-create.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcCreateService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PcCreateService = (function () {
    function PcCreateService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.authHeader = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.authHeader.append('Authorization', " " + this.currentUser.token);
        //Get the races on 
        this.getRaces();
    }
    ;
    PcCreateService.prototype.getRaces = function () {
        var _this = this;
        return this.http.get(this.serverUrl.baseUrl + '/race/index.json', { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.getCareers = function () {
        var _this = this;
        return this.http.get(this.serverUrl.baseUrl + '/career/index.json', { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    //Gets a list of all the specializations for the careerr
    PcCreateService.prototype.getCareerSpecializations = function (id) {
        var _this = this;
        return this.http.get(this.serverUrl.baseUrl + '/career/get-career-specializations/' + id + ".json", { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    //Gets a list of all the skills for the specialization
    PcCreateService.prototype.getCareerSpecializationSkills = function (id) {
        var _this = this;
        return this.http.get(this.serverUrl.baseUrl + '/career/get-career-specialization-skills/' + id + ".json", { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    //Gets a list of all the skills
    PcCreateService.prototype.getSkills = function (id) {
        var _this = this;
        return this.http.post(this.serverUrl.baseUrl + '/player/get-pc-skills.json', { id: id, headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    //Gets the pc xp
    PcCreateService.prototype.getPcXp = function (id) {
        var _this = this;
        return this.http.get(this.serverUrl.baseUrl + '/player/get-pc-xp/' + id + ".json", { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.createNewPc = function (name, race_id, career_id) {
        var _this = this;
        console.log(race_id);
        console.log(career_id);
        return this.http.post(this.serverUrl.baseUrl + '/player/pc-create.json', { pc: { user_id: this.currentUser.id, name: name, race_id: race_id, career_id: career_id } }, { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.addNewPcSpecialization = function (pc_id, spec_id, use_xp) {
        var _this = this;
        return this.http.post(this.serverUrl.baseUrl + '/player/set-specialization.json', { id: pc_id, spec_id: spec_id, use_xp: use_xp }, { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.increaseSkillRank = function (pcId, skillId, useXp) {
        var _this = this;
        return this.http.post(this.serverUrl.baseUrl + '/player/increase-skill-rank.json', { id: pcId, skill_id: skillId, use_xp: useXp }, { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.increaseAttribute = function (pcId, attribute) {
        var _this = this;
        return this.http.post(this.serverUrl.baseUrl + '/player/increase-attribute.json', { id: pcId, attribute: attribute }, { headers: this.authHeader })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.finalizeNewPc = function (pcId) {
        var _this = this;
        return this.http.post(this.serverUrl.baseUrl + '/player/finalize-pc.json', { id: pcId }, { headers: this.authHeader })
            .toPromise()
            .then()
            .catch(function (res) { return _this.handleError; });
    };
    PcCreateService.prototype.handleError = function (error) {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return PcCreateService;
}());
PcCreateService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], PcCreateService);

var _a, _b;
//# sourceMappingURL=pc-create.service.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-select/pc-select.component.html":
/***/ (function(module, exports) {

module.exports = "PC SELECT COMPONENT HTML\n<h3 *ngIf=\"error\">{{error}}</h3>\n<div *ngIf=\"pcSelected === false\" class=\"row\">\n    <div class=\"col-sm-4 select_character\" *ngFor='let pc of allPcs'>\n        <h3>{{pc.name}}</h3>\n        <p>\n            Race: {{pc.race_name}}<br>\n            Career: {{pc.career_name}}<br>\n            XP: {{pc.xp}}<br>\n            Credits:  {{pc.credits}}<br>\n            Wounds:  {{pc.wounds_current}}  Max:  {{pc.wounds_thresh}}\n        </p>\n            \n        <div><a class=\"text-center\" (click)='selectPc(pc)'>{{pc.name}}'s Destiny Awaits</a></div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/player/pc-select/pc-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcSelectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pc_select_service__ = __webpack_require__("../../../../../src/app/player/pc-select/pc-select.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PcSelectComponent = (function () {
    function PcSelectComponent(pcSelectService, router) {
        this.pcSelectService = pcSelectService;
        this.router = router;
        this.pcSelected = false;
        this.error = '';
        this.allPcs = [];
    }
    PcSelectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pcSelectService.getAllPcs()
            .then(function () {
            //Only load to show 'active' PCs from the all PC list
            _this.pcSelectService.allPcs.forEach(function (pc) {
                if (pc.status == 'active')
                    _this.allPcs.push(pc);
            });
        })
            .catch(function (res) {
            _this.error = res;
            if (res.status == 401)
                console.log("Res Status 401");
        });
    };
    PcSelectComponent.prototype.selectPc = function (pc) {
        this.pcSelectService.selectedPc = pc;
        this.pcSelected = true;
        this.router.navigateByUrl('/player/pc-view');
    };
    return PcSelectComponent;
}());
PcSelectComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'pc-select',
        template: __webpack_require__("../../../../../src/app/player/pc-select/pc-select.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__pc_select_service__["a" /* PcSelectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__pc_select_service__["a" /* PcSelectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], PcSelectComponent);

var _a, _b;
//# sourceMappingURL=pc-select.component.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-select/pc-select.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcSelectService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PcSelectService = (function () {
    function PcSelectService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.authHeader = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.authHeader.append('Authorization', " " + this.currentUser.token);
    }
    PcSelectService.prototype.getAllPcs = function () {
        var _this = this;
        //Clear out allPcs
        this.allPcs = [];
        return this.http.post(this.serverUrl.baseUrl + '/player/get-player-pcs.json', { headers: this.authHeader, id: this.currentUser.id }, { headers: this.authHeader }) //JSON.stringify({id: this.currentUser.id, auth_token: this.currentUser.token}), {headers: this.authHeader})
            .toPromise()
            .then(function (res) { return _this.allPcs = res.json(); })
            .catch(this.handleError);
    };
    PcSelectService.prototype.handleError = function (error) {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return PcSelectService;
}());
PcSelectService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], PcSelectService);

var _a, _b;
//# sourceMappingURL=pc-select.service.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-view/pc-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body#pcView\n{\n    background-color: #F5F5F5;\n\tfont-size: 10px;\n    min-width: 600px;\n}\n\ntable\n{\n    font-size: 10px;\n}\n\n.heading\n{\n    color: rgb(169, 69, 68);\n    font-family: 'STARWARS', sans-serif;\n}\n\n.emphasis\n{\n    color: rgb(160, 60, 68);\n    font-weight: bold;\n}\n\n.pc-section\n{\n    font-size: 10px;\n    background-color: white;\n    border-bottom: 1px solid rgb(200, 200, 200);\n    border-right: 1px solid rgb(200, 200, 200);\n    width: 100%;\n    margin: 5px;\n}\n\n.pc-overview-section\n{\n    width: 38%;\n    display: inline-block;\n}\n\n.pc-avatar\n{\n\twidth: 10%;\n\tmin-width: 100px;\n\tfloat: left;\n\tmargin: 10px;\n}\n\n\nul#character-nav\n{\n  background-color: tan;\n  margin-left: auto;\n  margin-right: auto;\n  max-width: 800px;\n  min-width: 320px;\n  height: 50px;\n}\n\nul#character-nav li\n{\n\tfont-family: 'Kalam', cursive;\n\tfont-size: 35px;\n  list-style: none;\n  float: left;\n  width: 25%;\n  height: 20px;\n  padding: 0px;\n  margin: 0px;\n}\n\nul#character-nav li a\n{\n\ttext-decoration: none;\n\tcolor: black;\n}\n\nul#character-nav li a:hover\n{\n\ttext-decoration: none;\n\tcolor: blue;\n}\n\nimg.pc-nav\n{\n\twidth: 100%;\n}\n\n.nav-icon\n{\n    width: 50px;\n}\n\n.nav-hidden\n{\n    display: none;\n}\n\n.nav-show\n{\n    display: block;\n}\n\nimg.dice\n{\n    float: right;\n    height: 16px;\n}\n\nimg.micro-dice\n{\n    height: 10px;\n    float: left;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/player/pc-view/pc-view.component.html":
/***/ (function(module, exports) {

module.exports = "<body id=\"pcView\">\n<div class=\"container-fluid\" id=\"characterselected\">\n    <img src=\"/assets/imperial_logo.jpg\" (click)=\"togglePcNavMenu()\" class=\"nav-icon\">\n    <h5>Menu</h5>\n    <h5><a href routerLink=\"/player\"> Exit </a></h5>\n    <div>\n    \t<ul id=\"character-nav\" class=\"{{pcNavClass}}\">\n    \t\t<li>\n                <img class=\"pc-nav\" (click)=\"setPcViewStage('overview')\" src=\"/assets/pc/pc_overview.png\">      \n            </li>\n    \t\t<li>\n                <img class=\"pc-nav\" (click)=\"setPcViewStage('skills')\" src=\"/assets/pc/pc_skills.png\">\n            </li>\n    \t\t<li>\n                <img class=\"pc-nav\" (click)=\"setPcViewStage('equipment')\" src=\"/assets/pc/pc_gear.png\">\n            </li>\n    \t\t<li>\n                <img class=\"pc-nav\" (click)=\"setPcViewStage('combat')\" src=\"/assets/pc/pc_combat.png\">\n            </li>\n    \t</ul>\n    </div>\n    \n    <!-- Character Over Section -->\n<div id=\"character_overview\" *ngIf=\"pcViewStage === 'overview'\">\n    <section class=\"pc-section\">\n        <img src={{selectedPc.avatar}} class=\"pc-avatar\">\n        <h2 class=\"heading\">{{selectedPc.name}}</h2>\n        <section class=\"pc-overview-section\">\n            <p>Race: {{selectedPc.race_name}}</p>\n            <p>Career: {{selectedPc.career_name}}</p>\n            <p>XP: {{selectedPc.xp}}</p>\n            <p>Credits: {{selectedPc.credits}}</p>\n        </section>\n        <section class=\"pc-overview-section\">\n            <p>Obligation: N/A</p>\n            <p>Duty: N/A</p>\n            <p>Force Balance: N/A</p>\n        </section>\n    </section>\n    \n    <section class=\"pc-section\">\n    <h2 class=\"heading\">HEALTH</h2>\n    <table class=\"table\">\n        <tr>\n            <td>Strain</td>\n            <td class=\"emphasis\">{{selectedPc.strain_current}} / {{selectedPc.strain_thresh}}</td>\n            <td>Soak</td>\n            <td class=\"emphasis\">{{selectedPc.brawn}}</td> <!-- + armor[0].soak}}</td> -->\n        </tr>\n        <tr>\n            <td>Wounds</td>\n            <td class=\"emphasis\">{{selectedPc.wounds_current}} / {{selectedPc.wounds_thresh}}</td>\n            <td>Defence</td>\n            <td class=\"emphasis\"><!--{{0 + armor[0].soak}} --></td>\n        </tr>\n    </table>\n    </section>\n\n    <section class=\"pc-section\">\n    <h2 class=\"heading\">ATTRIBUTES</h2>\n    <table class=\"table\">\n        <tr>\n            <td>Agility</td>\n            <td class=\"emphasis\">{{selectedPc.agility}}</td>\n            <td>Brawn</td>\n            <td class=\"emphasis\">{{selectedPc.brawn}}</td>\n            <td>Cunning</td>\n            <td class=\"emphasis\">{{selectedPc.cunning}}</td>\n\n        </tr>\n        <tr>\n            <td>Intellect</td>\n            <td class=\"emphasis\">{{selectedPc.intellect}}</td>\n            <td>Presence</td>\n            <td class=\"emphasis\">{{selectedPc.presence}}</td>\n            <td>Willpower</td>\n            <td class=\"emphasis\">{{selectedPc.willpower}}</td>\n        </tr>\n    </table>\n    </section>\n</div>\n\n<!-- Character Skills -->\n<div id=\"character_skills\" *ngIf=\"pcViewStage === 'skills'\">\n    <div class=\"col-sm-12\">\n    <section class=\"pc-section\">\n    <table class=\"table\">\n        <tr>\n            <td class=\"heading\">Skill</td>\n            <td class=\"heading\">Attribute</td>\n            <td class=\"heading text-right\">Rank</td>\n            <td class=\"heading text-right\">Career</td>\n            <td class=\"heading text-right\">Dice</td>\n        </tr>\n            <tr *ngFor=\"let skill of selectedPc.skills\" >\n                <td>{{skill.name}}</td>\n                <td>{{skill.attrib}}</td>\n                <td class=\"text-right emphasis\">{{skill.rank}}</td>\n                <td class=\"text-right emphasis\" *ngIf=\"skill.career === true\">*</td>\n                <td *ngIf=\"skill.career === false\"></td>\n                <td>\n                    <div class=\"float-right\" *ngFor=\"let die of skill.dice\">\n                        <img class=\"dice\" src=\"assets/{{die}}\" />\n                    </div>\n                </td>\n            </tr>\n    </table>\n    </section>\n    </div>\n</div>\n\n<!-- Character Equipment -->\n<div id=\"character_equipment\" *ngIf=\"pcViewStage === 'equipment'\">\n    <section class=\"pc-section\">\n    <h2 class=\"heading\">WEAPONS</h2>\n    <table class=\"table table-stripped\">\n    \t<thead>\n    \t\t<tr>\n    \t\t\t<td>Name</td>\n    \t\t\t<td>Skill</td>\n    \t\t\t<td>Damage</td>\n    \t\t\t<td>Critical</td>\n    \t\t\t<td>Range</td>\n    \t\t\t<td>Price</td>\n    \t\t\t<td>Special</td>\n    \t\t</tr>\n    \t</thead>\n    \t<tbody>\n    \t\t<tr *ngFor=\"let w of selectedPc.weapons\">\n    \t\t\t<td class=\"emphasis\">{{w.name}}</td>\n    \t\t\t<td>{{w.skill}}</td>\n    \t\t\t<td>{{w.damage}}</td>\n    \t\t\t<td>{{w.critical}}</td>\n    \t\t\t<td>{{w.range}}</td>\n    \t\t\t<td>{{w.price}}</td>\n    \t\t\t<td>{{w.special}}</td>\n    \t\t</tr>\n    \t</tbody>\n    </table>\n    </section>\n\n    <section class=\"pc-section\">\n    <h2 class=\"heading\">ARMOR</h2>\n    <table class=\"table table-stripped\">\n        <thead>\n            <tr>\n                <td>Name</td>\n                <td>Soak</td>\n                <td>Defense</td>\n                <td>Price</td>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let a of selectedPc.armors\">\n                <td class=\"emphasis\">{{a.name}}</td>\n                <td>{{a.soak}}</td>\n                <td>{{a.defense}}</td>\n                <td>{{a.price}}</td>\n            </tr>\n        </tbody>\n    </table>\n    </section>\n\n    <section class=\"pc-section\">\n    <h2 class=\"heading\">ITEMS</h2>\n    <table class=\"table table-stripped\">\n        <thead>\n            <tr>\n                <td>Name</td>\n                <td>Description</td>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let i of selectedPc.items\">\n                <td class=\"emphasis\">{{i.name}}</td>\n                <td>{{i.description}}</td>\n            </tr>\n        </tbody>\n    </table>\n    </section>\n</div>\n\n<!-- Character Combat -->\n<div id=\"character_combat\" *ngIf=\"pcViewStage === 'combat'\">\n    <div class=\"row\">\n        <div class=\"col-sm-6\">\n            <section class=\"pc-section\">\n                <h2 class=\"heading\">COMBAT STATS</h2>\n                <table class=\"table\">\n                    <tr>\n                        <td>Strain</td>\n                        <td class=\"emphasis\">{{selectedPc.strain_current}} / {{selectedPc.strain_thresh}}</td>\n                        <td>Soak</td>\n                        <!-- <td *ngIf=\"selectedPc.name === 'test'\" class=\"emphasis\">{{selectedPc.brawn + selectedPc.armors[0].soak}}</td> -->\n                        <td *ngIf=\"selectedPc.armors.length === 0\">{{selectedPc.brawn}}</td>\n                        <td *ngIf=\"selectedPc.armors.length > 0\">{{selectedPc.brawn + selectedPc.armors[0].soak}}</td>\n                    </tr>\n                    <tr>\n                        <td>Wounds</td>\n                        <td class=\"emphasis\">{{selectedPc.wounds_current}} / {{selectedPc.wounds_thresh}}</td>\n                        <td>Defence</td>\n                        <td *ngIf=\"selectedPc.armors[0] != null\" class=\"emphasis\">{{selectedPc.armors[0].defense}}</td>\n                        <td *ngIf=\"selectedPc.armors[0] === null\">0</td>\n                    </tr>\n                </table>\n            </section>\n\n            <section class=\"pc-section\">\n                <h2 class=\"heading\">ITEMS</h2>\n                <table class=\"table\">\n                    <thead>\n                        <tr>\n                            <td>Name</td>\n                            <td>Description</td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let i of selectedPc.items\">\n                            <td class=\"emphasis\">{{i.name}}</td>\n                            <td>{{i.description}}</td>\n                        </tr>\n                    </tbody>\n                </table>\n            </section>\n        </div>\n        \n        <div class=\"col-sm-6\">\n            <section class=\"pc-section\">\n                <h2 class=\"heading\">WEAPONS</h2>\n                <table class=\"table\">\n                    <thead>\n                        <tr>\n                            <td>Name</td>\n                            <td>Skill</td>\n                            <td>Damage</td>\n                            <td>Critical</td>\n                            <td>Range</td>\n                            <td>Special</td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let w of selectedPc.weapons\">\n                            <td class=\"emphasis\">{{w.name}}</td>\n                            <td>{{w.skill}}<br>\n                               <div>\n                                    <div *ngFor=\"let die of w.dice\">\n                                        <img class=\"micro-dice\" src=\"assets/{{die}}\" />\n                                    </div>\n                                </div>\n                            </td>\n                            \n                            <td>{{w.damage}}</td>\n                            <td>{{w.critical}}</td>\n                            <td>{{w.range}}</td>\n                            <td>{{w.special}}</td>\n                        </tr>\n                    </tbody>\n                </table>\n            </section>\n            \n                        <section class=\"pc-section\">\n                <h2 class=\"heading\">ARMOR</h2>\n                <table class=\"table\">\n                    <thead>\n                        <tr>\n                            <td>Name</td>\n                            <td>Soak</td>\n                            <td>Defence</td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let a of selectedPc.armors\">\n                            <td class=\"emphasis\">{{a.name}}</td>\n                            <td>{{a.soak}}</td>\n                            <td>{{a.defense}}</td>\n                        </tr>\n                    </tbody>\n                </table>\n            </section>\n        </div>\n    </div>\n</div>\n\n</div>\n\n</body>"

/***/ }),

/***/ "../../../../../src/app/player/pc-view/pc-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pc_select_pc_select_service__ = __webpack_require__("../../../../../src/app/player/pc-select/pc-select.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pc_view_service__ = __webpack_require__("../../../../../src/app/player/pc-view/pc-view.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PcViewComponent = (function () {
    function PcViewComponent(pcSelectService, pcViewService, router) {
        this.pcSelectService = pcSelectService;
        this.pcViewService = pcViewService;
        this.router = router;
        this.pcViewStage = "overview"; //What stage is the view on?
        this.pcNavClass = "nav-hidden"; //Stage of the nav bar
    }
    PcViewComponent.prototype.ngOnInit = function () {
        //Save the PC that was selected to work with
        this.selectedPc = this.pcSelectService.selectedPc;
        //If selectedPc == null, return to pc-select
        if (this.selectedPc == null)
            this.router.navigateByUrl("/player");
        this.setDice(this.selectedPc.skills.length);
        this.setWeaponDice();
        console.log(this.selectedPc);
    };
    PcViewComponent.prototype.togglePcNavMenu = function () {
        //Toggle the nav menu
        if (this.pcNavClass == "nav-hidden")
            this.pcNavClass = "nav-show";
        else
            this.pcNavClass = "nav-hidden";
    };
    PcViewComponent.prototype.setPcViewStage = function (view) {
        //Set the current view stage
        this.pcViewStage = view;
    };
    PcViewComponent.prototype.setWeaponDice = function () {
        var _this = this;
        //Loop through each weapon
        this.selectedPc.weapons.forEach(function (weapon) {
            //Loop through each selectedPc skill
            _this.selectedPc.skills.forEach(function (skill) {
                //if the weapon skill name == the current skill, push the dice to weapon
                if (weapon.skill == skill.name) {
                    weapon.dice = skill.dice;
                }
            });
        });
    };
    PcViewComponent.prototype.setDice = function (len) {
        //Set each die for each skill / skill check based of off rank and attribute
        for (var i = 0; i < len; i++) {
            this.selectedPc.skills[i].dice = new Array;
            //Attribute is greater than rank
            var attrib = this.selectedPc.skills[i].attrib;
            switch (attrib) {
                case "Agility":
                    if (this.selectedPc.agility >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.agility - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.agility; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.agility; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
                case "Brawn":
                    if (this.selectedPc.brawn >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.brawn - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.brawn; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.brawn; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
                case "Cunning":
                    if (this.selectedPc.cunning >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.cunning - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.cunning; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.cunning; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
                case "Intellect":
                    if (this.selectedPc.intellect >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.intellect - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.intellect; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.intellect; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
                case "Presence":
                    if (this.selectedPc.presence >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.presence - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.presence; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.presence; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
                case "Willpower":
                    if (this.selectedPc.willpower >= this.selectedPc.skills[i].rank) {
                        for (var j = 0; j < this.selectedPc.skills[i].rank; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.willpower - this.selectedPc.skills[i].rank; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    else {
                        for (var j = 0; j < this.selectedPc.willpower; j++) {
                            this.selectedPc.skills[i].dice.push("Proficient.png");
                        }
                        for (var h = 0; h < this.selectedPc.skills[i].rank - this.selectedPc.willpower; h++) {
                            this.selectedPc.skills[i].dice.push("Standard.png");
                        }
                    }
                    break;
            }
        }
        return 1;
    };
    ;
    return PcViewComponent;
}());
PcViewComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        template: __webpack_require__("../../../../../src/app/player/pc-view/pc-view.component.html"),
        styles: [__webpack_require__("../../../../../src/app/player/pc-view/pc-view.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__pc_select_pc_select_service__["a" /* PcSelectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__pc_select_pc_select_service__["a" /* PcSelectService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__pc_view_service__["a" /* PcViewService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__pc_view_service__["a" /* PcViewService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _c || Object])
], PcViewComponent);

var _a, _b, _c;
//# sourceMappingURL=pc-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/player/pc-view/pc-view.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PcViewService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PcViewService = (function () {
    function PcViewService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        console.log(this.currentUser);
        this.authHeader = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.authHeader.append('Authorization', " " + this.currentUser.token);
    }
    return PcViewService;
}());
PcViewService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], PcViewService);

var _a, _b;
//# sourceMappingURL=pc-view.service.js.map

/***/ }),

/***/ "../../../../../src/app/player/player-login/player-login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form\n{\n    width: 300px;\n    margin-left: auto;\n    margin-right: auto;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/player/player-login/player-login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"player_signin\" ng-hide=\"signed_in\">\n    <h2 class=\"text-center\">Please sign in</h2>\n    <h3 *ngIf=\"error\">{{error}}</h3>\n\t<form id=\"pc-sign-in\">\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"email\">Email</label>\n\t\t\t<input class=\"form-control\" type=\"text\" name=\"email\" [(ngModel)]=\"user.email\" placeholder=\"User Email\">\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"password\">Password</label>\n\t\t\t<input class=\"form-control\" type=\"password\" name=\"password\" [(ngModel)]=\"user.password\" placeholder=\"Password\">\n\t\t</div>\n\t\t<button type=\"submit\" class=\"btn btn-default\" (click)=\"login()\">Login</button>\n\t</form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/player/player-login/player-login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerLoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_player_authentication_service__ = __webpack_require__("../../../../../src/app/services/player-authentication.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerLoginComponent = (function () {
    function PlayerLoginComponent(router, playerAuthService) {
        this.router = router;
        this.playerAuthService = playerAuthService;
        this.error = '';
        this.user = {};
    }
    PlayerLoginComponent.prototype.ngOnInit = function () {
    };
    PlayerLoginComponent.prototype.login = function () {
        var _this = this;
        this.playerAuthService.login(this.user.email, this.user.password)
            .then(function (player) {
            console.log(player);
            localStorage.setItem('currentUser', JSON.stringify({ id: player.user_id, name: player.name, email: player.email, token: player.auth_token }));
            console.log(localStorage.getItem('currentUser'));
            _this.router.navigate(['/player']);
            //this.playerComponent.currentPlayer = player;
        })
            .catch(function (response) {
            _this.error = "Error logging in!";
        });
        /*this.playerAuthService.login(this.user.email, this.user.password) //('example@mail.com', 'password')
        .subscribe(result =>
        {
            console.log(result);
            if (result === true)
            {
                //login successful
                this.router.navigate(['/player']);
            }
            else
            {
                console.log("Error");
                //login failed
                this.error = 'Username or password is incorrect';
            }
        });*/
    };
    return PlayerLoginComponent;
}());
PlayerLoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        template: __webpack_require__("../../../../../src/app/player/player-login/player-login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/player/player-login/player-login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_player_authentication_service__["a" /* PlayerAuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_player_authentication_service__["a" /* PlayerAuthenticationService */]) === "function" && _b || Object])
], PlayerLoginComponent);

var _a, _b;
//# sourceMappingURL=player-login.component.js.map

/***/ }),

/***/ "../../../../../src/app/player/player.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/player/player.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n<div class=\"container-fluid\">\n   \t<h1 class=\"text-center\">Welcome {{currentPlayer.name}}</h1>\n    <h6 class=\"text-center\"><a href (click)=\"logOut()\">Log Out</a></h6>\n\t<h6 class=\"text-center\"><a href (click)=\"editPlayer()\">Edit player profile</a></h6>\n\t<h4 class=\"text-center\"><a routerLink='/player/pc-create'>+ New Character +</a></h4>\n\t\n\t<pc-select></pc-select>\n    \n</div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/player/player.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_player__ = __webpack_require__("../../../../../src/app/models/player.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pc_select_pc_select_service__ = __webpack_require__("../../../../../src/app/player/pc-select/pc-select.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_player_authentication_service__ = __webpack_require__("../../../../../src/app/services/player-authentication.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PlayerComponent = (function () {
    function PlayerComponent(router, playerAuthService, pcSelectService) {
        this.router = router;
        this.playerAuthService = playerAuthService;
        this.pcSelectService = pcSelectService;
        this.currentPlayer = new __WEBPACK_IMPORTED_MODULE_2__models_player__["a" /* Player */];
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentPlayer.user_id = currentUser['id'];
        this.currentPlayer.name = currentUser['name'];
        this.currentPlayer.email = currentUser["email"];
        console.log(this.currentPlayer);
    }
    PlayerComponent.prototype.logOut = function () {
        //Log out the current player and return to the login screen
        this.playerAuthService.logout();
        this.router.navigate(['/player-login']);
    };
    return PlayerComponent;
}());
PlayerComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'player',
        template: __webpack_require__("../../../../../src/app/player/player.component.html"),
        styles: [__webpack_require__("../../../../../src/app/player/player.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_player_authentication_service__["a" /* PlayerAuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_player_authentication_service__["a" /* PlayerAuthenticationService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__pc_select_pc_select_service__["a" /* PcSelectService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__pc_select_pc_select_service__["a" /* PcSelectService */]) === "function" && _c || Object])
], PlayerComponent);

var _a, _b, _c;
//# sourceMappingURL=player.component.js.map

/***/ }),

/***/ "../../../../../src/app/services/gm-authentication.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GMAuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GMAuthenticationService = (function () {
    function GMAuthenticationService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        //Set token if saved in local storage
        var currentGM = JSON.parse(localStorage.getItem('currentGM'));
        this.token = currentGM && currentGM.token;
    }
    GMAuthenticationService.prototype.login = function (email, password) {
        return this.http.post(this.serverUrl.baseUrl + '/gm_authenticate', { email: email, password: password }, { withCredentials: true })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    GMAuthenticationService.prototype.handleError = function (error) {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    /*login(email, password): Observable<boolean>
    {
        //Call server to login
        //TODO replace URL
        return this.http.post('http://localhost:4000/authenticate', {email: email, password: password })
        .map((response: Response) =>
        {
            //Got a response.  If there is an auth_token, save in token
            let token = response.json() && response.json().auth_token;
            if(token)
            {
                //Save the token into this.token and save into localStorage as 'currentUser'
                this.token = token;

                localStorage.setItem('currentUser', JSON.stringify({email: email, token: token}));
                return true;
            }
            else
            {
                //There was an error.  Print out to console
                console.log(response.json().error);
                return false
            }
        });
    }*/
    GMAuthenticationService.prototype.logout = function () {
        //Logout current user.  Clear token and remove 'currentUser' from localStorage
        this.token = null;
        localStorage.removeItem('currentGM');
    };
    return GMAuthenticationService;
}());
GMAuthenticationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], GMAuthenticationService);

var _a, _b;
//# sourceMappingURL=gm-authentication.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/player-authentication.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerAuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__ = __webpack_require__("../../../../../src/app/services/server-url.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PlayerAuthenticationService = (function () {
    function PlayerAuthenticationService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        //Set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
    PlayerAuthenticationService.prototype.login = function (email, password) {
        return this.http.post(this.serverUrl.baseUrl + '/authenticate', { email: email, password: password })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    PlayerAuthenticationService.prototype.handleError = function (error) {
        //console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    /*login(email, password): Observable<boolean>
    {
        //Call server to login
        //TODO replace URL
        return this.http.post('http://localhost:4000/authenticate', {email: email, password: password })
        .map((response: Response) =>
        {
            //Got a response.  If there is an auth_token, save in token
            let token = response.json() && response.json().auth_token;
            if(token)
            {
                //Save the token into this.token and save into localStorage as 'currentUser'
                this.token = token;

                localStorage.setItem('currentUser', JSON.stringify({email: email, token: token}));
                return true;
            }
            else
            {
                //There was an error.  Print out to console
                console.log(response.json().error);
                return false
            }
        });
    }*/
    PlayerAuthenticationService.prototype.logout = function () {
        //Logout current user.  Clear token and remove 'currentUser' from localStorage
        this.token = null;
        localStorage.removeItem('currentUser');
    };
    return PlayerAuthenticationService;
}());
PlayerAuthenticationService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_server_url_service__["a" /* ServerUrlService */]) === "function" && _b || Object])
], PlayerAuthenticationService);

var _a, _b;
//# sourceMappingURL=player-authentication.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/server-url.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServerUrlService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServerUrlService = (function () {
    function ServerUrlService() {
        //this.baseUrl = "https://swrpg-api-nlblunt.c9users.io";
        //this.baseUrl = "http://swrpgapi.rubywebs.net";
        this.baseUrl = "";
    }
    return ServerUrlService;
}());
ServerUrlService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], ServerUrlService);

//# sourceMappingURL=server-url.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map