# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171024200751) do

  create_table "armors", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "defense"
    t.integer  "soak"
    t.integer  "price"
    t.text     "notes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "armors_pcs", force: :cascade do |t|
    t.integer "armor_id"
    t.integer "pc_id"
  end

  create_table "careers", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "careers_pcs", id: false, force: :cascade do |t|
    t.integer "career_id"
    t.integer "pc_id"
  end

  create_table "careers_skills", id: false, force: :cascade do |t|
    t.integer "career_id"
    t.integer "skill_id"
  end

  create_table "gms", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.integer  "price"
    t.text     "description"
    t.text     "notes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "items_pcs", force: :cascade do |t|
    t.integer "item_id"
    t.integer "pc_id"
  end

  create_table "pcs", force: :cascade do |t|
    t.string   "name"
    t.integer  "xp"
    t.integer  "credits"
    t.integer  "brawn"
    t.integer  "agility"
    t.integer  "intellect"
    t.integer  "cunning"
    t.integer  "willpower"
    t.integer  "presence"
    t.integer  "wounds_thresh"
    t.integer  "wounds_current"
    t.integer  "strain_thresh"
    t.integer  "strain_current"
    t.integer  "critical"
    t.integer  "soak"
    t.string   "obligation_type"
    t.integer  "obligation_amount"
    t.integer  "user_id"
    t.integer  "race_id"
    t.integer  "career_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "avatar"
    t.string   "status"
    t.index ["career_id"], name: "index_pcs_on_career_id"
    t.index ["race_id"], name: "index_pcs_on_race_id"
  end

  create_table "pcs_sessions", force: :cascade do |t|
    t.integer "pc_id"
    t.integer "session_id"
  end

  create_table "pcs_skills", force: :cascade do |t|
    t.integer "pc_id"
    t.integer "skill_id"
    t.integer "rank",     default: 0
    t.boolean "cskill",   default: false
  end

  create_table "pcs_specializations", id: false, force: :cascade do |t|
    t.integer "pc_id"
    t.integer "specialization_id"
  end

  create_table "pcs_weapons", force: :cascade do |t|
    t.integer "pc_id"
    t.integer "weapon_id"
  end

  create_table "races", force: :cascade do |t|
    t.string   "name"
    t.integer  "xp"
    t.integer  "brawn"
    t.integer  "agility"
    t.integer  "intellect"
    t.integer  "cunning"
    t.integer  "willpower"
    t.integer  "presence"
    t.integer  "wounds_thresh"
    t.integer  "strain_thresh"
    t.string   "bonus"
    t.text     "description"
    t.string   "avatar"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string   "name"
    t.string   "attrib"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "skills_specializations", id: false, force: :cascade do |t|
    t.integer "skill_id"
    t.integer "specialization_id"
  end

  create_table "specializations", force: :cascade do |t|
    t.string   "name"
    t.text     "descriptin"
    t.integer  "career_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "urls", force: :cascade do |t|
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "weapons", force: :cascade do |t|
    t.string   "name"
    t.integer  "skill_id"
    t.integer  "damage"
    t.integer  "critical"
    t.string   "range"
    t.integer  "price"
    t.string   "special"
    t.text     "description"
    t.text     "notes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
