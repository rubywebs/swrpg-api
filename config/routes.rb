Rails.application.routes.draw do
  get 'get_url' => 'gurl#get_url'
  post 'add_url' => 'gurl#add_url'

  post 'authenticate', to: 'authentication#authenticate'
  post 'gm_authenticate', to: 'gm_authentication#authenticate'
  
  #Resources
  resources :weapon
  resources :armor
  resources :item

  #GM CONTROLLER ROUTES
  get 'gm/gm_check' => "gm#gm_check"
  post 'gm/get-all-pcs' => "gm#get_all_pcs"
  post 'gm/modify_pc' => "gm#modify_pc"
  post 'gm/pc_modify_strain' => "gm#pc_modify_strain"
  post 'gm/pc_modify_wounds' => "gm#pc_modify_wounds"
  post 'gm/get_session_pcs' => "gm#get_session_pcs"
  post 'gm_add_session_pcs' => "gm#add_session_pcs"
  
  #CAREER CONTROLLER ROUTES
  get 'career/index'
  get "career/get-career-specializations/:id" => "career#get_career_specializations"
  #get "career/get_all_specializations" => "career#get_all_specializations"
  get "career/get-career-specialization-skills/:id" => "career#get_specialization_career_skills"
  
  #PLAYER CONTROLLER ROUTES
  resources :player
  #get "player/get_pc/:id" => "player#get_pc"
  post "player/get-player-pcs" => "player#get_player_pcs"
  #get "users/player_check" => "player#player_check"
  post "player/pc-create" => "player#create_pc"
  #post "player/delete_pc" => "player#delete_pc"
  get "player/get-pc-xp/:id" => "player#get_pc_xp"
  post "player/get-pc-skills" => "player#get_pc_skills"
  #get "player/get_pc_career_skills/:id" => "player#get_pc_career_skills"
  post "player/increase-skill-rank" => "player#increase_skill_rank"
  post "player/increase-attribute" => "player#increase_attribute"
  post "player/set-specialization" => "player#set_specialization"
  post "player/finalize-pc" => "player#finalize_pc"
  #post "player/get_pc_weapons" => "player#get_pc_weapons"
  #post "player/get_pc_armor" => "player#get_pc_armor"
  #post "player/get_pc_items" => "player#get_pc_items"
  #post "player/add_weapon" => "player#add_weapon"
  #post "player/delete_weapon" => "player#delete_weapon"
  #post "player/add_armor" => "player#add_armor"
  #post "player/delete_armor" => "player#delete_armor"
  #post "player/add_item" => "player#add_item"
  #post "player/delete_item" => "player#delete_item"
  
  #RACE CONTROLLER ROUTES
  get 'race/index'

  #GAME CONTROLLER ROUTES (GAME SESSION)
  #get 'game/get_all_sessions' => "game#get_all_sessions"
  #post 'game/create_session' => "game#create_session"
  #post 'game/restore_session' => "game#restore_session"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
